---
layout: cv
title: Markus Wolf Heidemann's CV
---

# Markus Heidemann

Security Engineer, DevOps, Developer, Bachelor of (Computer) Science

<div id="webaddress">
<a href="m.heidemann@posteo.de">m.heidemann@posteo.de</a>
<a href="tel:+4915770313787">+49 157 70 313 787</a>
<p> available after 04/2022 </p>
<!--- | <a href="http://en.wikipedia.org/wiki/Isaac_Newton">My wikipedia page</a> --->
</div>

## Currently

`04/2021 - now`

- Enrolled in TU Darmstadt, general master computer science.
  - focus on broadening view from one-track specialist to communicator
  - machine learning: I seek the ability to estimate in which places machine learning is the correct tool
    - research showed traditional software implementation lead to better results + has auditability / can be tested

`05/2021 - 04/2022`

- Research Assistent in usable security & Privacy - FANCY project - University of Applied Science Darmstadt (hda) (till 04/2022)
  - analysis of open source projects
  - focus: identify development activities which lead to improved _usable_ security/privacy
  - evaluation of scoring methodologies

### Specialized in

- IT security:
  - focus of my bachelor studies + some practice
- Application development
  - Bachelor studies cooperation company: Single-Page-Application development
  - web & server side development experience
- linux: daily usage, scripting for private side projects

### Programming Experience

- Most comfortable with c#, linq and (Ms)Sql; used them extensively during full time backend development (>3 years) at NetzWerkPlan GmbH
- From frontend development (2015,2016) at NetzWerkPlan, I am familiar with Javascript, Typescript, CSS, Html and Pug.
- Languages used in one or more projects: bash, python, c++, assembler, dart, TLA+, specflow, Gherkin

### Research interests

- security
  - Which development practices lead to secure applications?
- open source: generally I am a fan of the open source ecosystem
  - Custom modification (e.g. security fixes / new features) will always be possible; even if the manufacturer does not exist anymore
  - societal benefit through reuse of software instead of reinventing the wheel
- sustainable development
  - which practices can be applied to reduce pollutant emission from the IT-Industry?
  - e.g. do not build every commit, only build the latest commit
- private data protection
  - A big underlying motivator for security, Linux usage and open source

## Conferences

- Frankfurt Developerday 2018 (out of own interest)
  - strong memories: value of code maintainability, documentation generated from code, testing generally
- 35c3 (2018), 36c3 (2019):

## Education

`June 2012`
__Heinrich-Emanuel-Merck-Schule, Darmstadt.__

- Abitur with additional qualification 'Data Processing' (= first computer science courses)

`09/2012 - 11/2014`
__Duale Hochschule Baden-Württemberg, Mannheim__

- Cooperative Studies of 'Information Technology' with Sirona Dental Services GmbH in Bensheim
(aborted)
- Studies about the meeting spot of hardware and software (e.g. compiler building, digital technique, embedded systems)

`11/2014 - 02/2015`
__Recovering from Illness__

`03-08/2015`
__Internship at NetzWerkPlan GmbH__

- I wanted to work (and not wait till the start of the education)
- introduction to javascript, css and html
- development of pure fronted features for an AngularJs application
- got to know Scrum practices as planning poker, sprints, etc.

`09/2015 - 06/2017`
__Education as 'IT specialist for application development'__

- normal education time is 3 years, I shortended to two years
- starting in FE (first year), worked my way into the backend/server side (during and after second year)
- working on features as the other (academic) colleagues

`06-09/2017`
__Fulltime employment at NetzWerkPlan GmbH__

- no change in tasks

`10/2017 - 04/2021`
__Bachelor of (Computer) Science with area of specialisation 'IT security', University of Applied Science Darmstadt (hda)__

- __beside normal computer science bachelor__
  - IT-compliance, software security (ISO27001), cryptography, network security (course from norway approved)
  - penetration testing, it security management
__practical experience__
  - Security analysis of the (productive and development) infrastructure of the main company product/software (based on ISO27001)
    - identification of vulnerabilities, estimation of occurrence probability -> risk analysis with company owner + decision which measures should be implemented in which priority
  - analysis of delayed release causes + implementation of a behavior driven development process

`08-12/2019`
__Semester Abroad at Norwegian University of Science and Technology, Norway Gjøvik__

- taking part in the local capture-the-flag (CTF) playground
- Network Security (10 ECTS, Grade B)
  - building of network for a small company
  - managed Cisco switch configuration, usage of VLans, (wireless) encryption
  - setup of organizational IT-security
- Introduction to Digital Forensics (7.5 ECTS, Grade C)
- Critical Infrastructure Security (7.5 ECTS, Grade C)
- Norwegian Language and Culture for International S (5 ECTS, Grade Passed)

`04/2021 - now`
__Master of (Computer) Science, TU Darmstadt__

- Data Mining and Machine Learning
- TK3: Ubiquitous / Mobile Computing
- lecture series ``Digitales Business und Start-ups''

current semester Physics I, Electronics, Electronics Lab, Wireless Network for Emergency Response: Fundamentals, Design, and Build-up from Scratch

## Volunteer Work

`10/2017 - 06/2019`
__Amnesty International High School Group, TU Darmstadt__

- till `04/2018` normal group member
- afterwards 'group speaker' with e.g. responsibility for external communication, giving impulses into the group
- generally working to inform people about human rights violations and collect signatures against those.

`04/2018 - 06/2019`
__Active in computer science student association__

- generally helping new or lost students around
- organizing protest ~study with dignity
- address problems with course requirements

`04/2018 - 04/2021`
__Student member of the computer science audit committee, hda (computer science department)__

- evalualte and discuss general exam handling
- individual student case handling (e.g. more time for bachelor thesis, handling of fraud)

`08-12/2019`
__Semester Abroad at Norwegian University of Science and Technology, Norway Gjøvik__

- active in student welfare
  (Position: Vice President International Student Union in Gjøvik)
  - organize events for students
    - e.g. German night, three day trip to a national park
  - handle bills, apply for funding, draw up budget

`01/2021 ~ 04/2021`
__active in open source project bbb@scale__

- open source project founded of hda members to solve challenges of digital teaching (demo: rooms.h-da.de)
- parallel to bachelor thesis
- contribution: development of continous docker deployment to hub.docker.com

`12/2020 - now`
__active in 'Klimaliste Darmstadt-Dieburg'__

- new german party `more green than *Die Grünen*'
- pushing environmental sustainability into policies since then

<!-- ### Footer
Last updated: Januar 2022 -->
